# frozen_string_literal: true

require 'app/point.rb'

class Rectangle
  attr_accessor :a, :b

  def initialize(a, b, color: 'Red')
    @a = a
    @b = b
    @color = color
  end

  def draw
    [a.x, a.y, b.x, b.y]
  end
end
