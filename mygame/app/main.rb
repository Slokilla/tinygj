# frozen_string_literal: true

require 'app/point.rb'
require 'app/rectangle.rb'
require 'app/raquette.rb'
# frozen_string_literal: true

def tick(args)
  args.state.raquette ||= Raquette.new
  args.outputs.solids << args.state.raquette.draw
  checkInputs args
end

def checkInputs(args)
  args.state.raquette.moveLeft if args.inputs.left
  args.state.raquette.moveRight if args.inputs.right
end
