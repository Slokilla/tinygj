require 'app/point.rb'
require 'app/rectangle.rb'

class Raquette < Rectangle
  attr_accessor :pos,
                :len,
                :hei,
                :speed

  def initialize
    @speed = 5
    @len = 200
    @hei = 50
    @pos = Point.new(0, 0)
    b = Point.new(pos.x + len, pos.y + hei)
    super(@pos, b)
  end

  def moveLeft
    log_info "moving"
    @pos.x = 0 if (@pos.x - @speed).negative?
    move -1 * @speed
  end

  def moveRight
    log_info "moving"
    @pos.x = 1280-len if @pos.x + @speed > 1280-len
    move @speed
  end

  private

  def move(speed)
    log_info "#{@pos.x} #{@speed}"
    @pos.x += speed
  end
end
